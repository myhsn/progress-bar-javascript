(function readyJS(win, doc){
    'use strict';
    let form1 = doc.querySelector('#form1');

    function sendForm(e){
        e.preventDefault();
        let ajax = new XMLHttpRequest();
        let formData = new FormData(form1);
        let btn = doc.querySelector('#btn');
        let result = doc.querySelector('#result');
        let progress = doc.querySelector('#progress');
        
        ajax.open('POST', 'progressbar.php');
        ajax.onloadstart = function () {
            btn.value = 'Carregando...';
        };
        ajax.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                progress.style.width = ((e.loaded * 100)/e.total) + '%';
                progress.innerHTML = ((e.loaded * 100)/e.total).toFixed() + '%';
                console.log(e.loaded + ' / ' + e.total);
            }
        };
        ajax.onloadend = function () {
            // progress.style.display = 'none'; // Ocultar a barra
            btn.value = 'Salvar';
            result.innerHTML = 'Arquivo enviado!';
        };
        ajax.send(formData);
    }
    
    form1.addEventListener('submit', sendForm, false);



})(window, document);